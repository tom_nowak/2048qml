import QtQuick 2.0
import "Parameters.js" as Parameters

Grid {
    id: board
    anchors.fill: parent
    anchors.margins: spacing
    rows: Parameters.BOARD_SIZE
    columns: Parameters.BOARD_SIZE
    spacing: (size * Parameters.BOARD_MARGINS_COEFFICIENT) / (Parameters.BOARD_SIZE + 1)

    property real size: Math.min(parent.width, parent.height)
    readonly property int totalFieldsNumber: Parameters.BOARD_SIZE * Parameters.BOARD_SIZE
    property int freeFieldsNumber: totalFieldsNumber
    property bool highestNumberReached: false
    property bool anythingMovedInThisTurn: false
    signal gameEnd(bool victory)

    function atIndex(index) {
        return repeater.itemAt(index)
    }

    function at(position) {
        return atIndex(position[0] + position[1] * Parameters.BOARD_SIZE)
    }

    function isBeingAnimated() {
        for(var i = 0; i < totalFieldsNumber; ++i) {
            if(atIndex(i).animationRunning)
                return true
        }
        return false
    }

    function endTurn() {
        if(highestNumberReached) {
            gameEnd(true)
        }
        if(freeFieldsNumber > 0 && anythingMovedInThisTurn) {
            spawnNewNumber()
        }
    }

    function randomFreeField() {
        var index = Math.floor(Math.random() * freeFieldsNumber)
        for(var i = 0; i < totalFieldsNumber; ++i) {
            if(atIndex(i).isFree()) {
                if(index === 0)
                    return atIndex(i)
                index -= 1
            }
        }
    }

    function spawnNewNumber(number) {
        if(number === undefined)
            number = 2
        randomFreeField().number = number
        freeFieldsNumber -= 1
    }

    function move(from, to) {
        at(to).number = at(from).number
        at(from).number = 0
        anythingMovedInThisTurn = true
    }

    function merge(from, to) {
        at(to).number *= 2
        at(from).number = 0
        if(at(to).number === Parameters.NUMBER_FINISHING_GAME) {
            highestNumberReached = true
        }
        freeFieldsNumber += 1
        anythingMovedInThisTurn = true
    }

    function moveLeft() {
        anythingMovedInThisTurn = false
        for(var y = 0; y < Parameters.BOARD_SIZE; ++y) {
            moveLine([0, y], [1, 0])
        }
        endTurn()
    }

    function moveRight() {
        anythingMovedInThisTurn = false
        for(var y = 0; y < Parameters.BOARD_SIZE; ++y) {
            moveLine([Parameters.BOARD_SIZE - 1, y], [-1, 0])
        }
        endTurn()
    }

    function moveUp() {
        anythingMovedInThisTurn = false
        for(var x = 0; x < Parameters.BOARD_SIZE; ++x) {
            moveLine([x, 0], [0, 1])
        }
        endTurn()
    }

    function moveDown() {
        anythingMovedInThisTurn = false
        for(var x = 0; x < Parameters.BOARD_SIZE; ++x) {
            moveLine([x, Parameters.BOARD_SIZE - 1], [0, -1])
        }
        endTurn()
    }

    function moveLine(startingPosition, direction) {
        compressLine(startingPosition, direction)
        mergeLine(startingPosition, direction)
        compressLine(startingPosition, direction)
    }

    function firstOccupiedPosition(startingPostion, direction, maxCount) {
        if(maxCount === undefined) {
            maxCount = Parameters.BOARD_SIZE
        }
        var position = startingPostion.slice()
        for(var i = 0; i < maxCount; ++i) {
            if(!at(position).isFree())
                return position
            position[0] += direction[0]
            position[1] += direction[1]
        }
        return [position[0] + direction[0], position[1] + direction[1]]
    }

    function firstFreePosition(startingPostion, direction, maxCount) {
        if(maxCount === undefined) {
            maxCount = Parameters.BOARD_SIZE
        }
        var position = startingPostion.slice()
        for(var i = 0; i < maxCount; ++i) {
            if(at(position).isFree())
                return position
            position[0] += direction[0]
            position[1] += direction[1]
        }
        return [position[0] + direction[0], position[1] + direction[1]]
    }

    function compressLine(startingPostion, direction) {
        var elementsCount = countElements(startingPostion, direction)
        for(var i = 0; i < elementsCount; ++i) {
            var position = [startingPostion[0] + i*direction[0], startingPostion[1] + i*direction[1]]
            if(at(position).isFree()) {
                var occupiedPosition = firstOccupiedPosition(position, direction, Parameters.BOARD_SIZE - i)
                move(occupiedPosition, position)
            }
        }
    }

    function mergeLine(startingPostion, direction) {
        var elementsCount = countElements(startingPostion, direction)
        for(var i = 1; i < elementsCount; ++i) {
            var position1 = [startingPostion[0] + (i-1)*direction[0], startingPostion[1] + (i-1)*direction[1]]
            var position2 = [startingPostion[0] + i*direction[0], startingPostion[1] + i*direction[1]]
            if(at(position1).number !== 0 && at(position1).number === at(position2).number) {
                merge(position2, position1)
            }
        }
    }

    function countElements(startingPostion, direction, maxCount) {
        if(maxCount === undefined) {
            maxCount = Parameters.BOARD_SIZE
        }
        var position = startingPostion.slice()
        var count = 0
        for(var i = 0; i < maxCount; ++i) {
            if(!at(position).isFree())
                count += 1
            position[0] += direction[0]
            position[1] += direction[1]
        }
        return count
    }

    Repeater {
        id: repeater
        model: board.totalFieldsNumber
        Cell {
            size: board.size * (1.0 - Parameters.BOARD_MARGINS_COEFFICIENT) / Parameters.BOARD_SIZE
        }
    }
}
