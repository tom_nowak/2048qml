import QtQuick 2.0
import QtQuick.Window 2.0
import "Parameters.js" as Parameters

Window {
    id: window
    width: 320
    height: 320
    minimumWidth: 100
    minimumHeight: 100
    visible: true
    title: "2048"

    Rectangle {
        id: application
        anchors.fill: parent
        focus: true
        color: "darkgray"
        property bool gameEnded: false

        Keys.onPressed: {
            if(board.isBeingAnimated()) {
                event.accepted = true
                return
            }
            if(gameEnded)
                Qt.quit()
            switch(event.key) {
                case Qt.Key_Left:
                    event.accepted = true
                    board.moveLeft()
                    break
                case Qt.Key_Right:
                    event.accepted = true
                    board.moveRight()
                    break
                case Qt.Key_Up:
                    event.accepted = true
                    board.moveUp()
                    break
                case Qt.Key_Down:
                    event.accepted = true
                    board.moveDown()
                    break
                case Qt.Key_Escape:
                    event.accepted = true
                    Qt.quit()
            }
        }
        Board {
            id: board
            onGameEnd: {
                if(victory)
                    endGameText.text = "You won!"
                else
                    endGameText.text = "You lost!"
                application.gameEnded = true
                endGameMessage.opacity = 1.0
            }
        }
        Rectangle {
            id: endGameMessage
            anchors.fill: parent
            color: "white"
            opacity: 0.0

            Behavior on opacity {
                NumberAnimation { duration: Parameters.END_GAME_ANIMATION_DURATION }
            }

            Text {
                id: endGameText
                width: parent.width * Parameters.TEXT_SIZE_COEFFICIENT
                height: parent.height * Parameters.TEXT_SIZE_COEFFICIENT
                anchors.centerIn: parent
                font.pixelSize: parent.height * Parameters.TEXT_SIZE_COEFFICIENT
                font.bold: true
                fontSizeMode: Text.Fit
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                text: ""
            }
        }

    }

    Component.onCompleted: {
        board.spawnNewNumber()
        board.spawnNewNumber()
    }
}
