.pragma library

var BOARD_SIZE = 4;
var BOARD_MARGINS_COEFFICIENT = 0.1;
var CELL_RADIUS_COEFFICIENT = 0.15;
var TEXT_SIZE_COEFFICIENT = 0.9;
var NUMBER_ANIMATION_DURATION = 300;
var END_GAME_ANIMATION_DURATION = 1500;
var NUMBER_FINISHING_GAME = 64; // temporary - to quickly show results, in normal case should be 2048

function cellColor(number) {
    if(number <= 0)         return "#e0e0e0"
    else if(number <= 2)    return "#eee4da"
    else if(number <= 4)    return "#ede0c8"
    else if(number <= 8)    return "#f2b179"
    else if(number <= 16)   return "#f59563"
    else if(number <= 32)   return "#f67c5f"
    else if(number <= 64)   return "#f65e3b"
    else if(number <= 128)  return "#edcf72"
    else if(number <= 256)  return "#edcc61"
    else if(number <= 512)  return "#edc850"
    else if(number <= 1024) return "#edc53f"
    else                    return "#edc22e"
}
