import QtQuick 2.0
import "Parameters.js" as Parameters

Rectangle {
    property real size
    property int number: 0
    property int animatedNmber: number
    property alias animationRunning: animation.running

    width: size
    height: size
    radius: size * Parameters.CELL_RADIUS_COEFFICIENT
    color: Parameters.cellColor(number)

    function isFree() {
        return number === 0
    }

    Behavior on animatedNmber {
        NumberAnimation {
            id: animation
            duration: Parameters.NUMBER_ANIMATION_DURATION
        }
    }

    Text {
        width: parent.width * Parameters.TEXT_SIZE_COEFFICIENT
        height: parent.height * Parameters.TEXT_SIZE_COEFFICIENT
        anchors.centerIn: parent
        font.pixelSize: parent.height * Parameters.TEXT_SIZE_COEFFICIENT
        font.bold: true
        fontSizeMode: Text.Fit
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        text: parent.animatedNmber
        opacity: Math.min(1.0, parent.animatedNmber/2.0)
    }
}
